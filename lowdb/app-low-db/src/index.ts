const app = require('./app');
import {crearConexion} from './base-lowdb/base-datos';

crearConexion();
app.listen(3000);
console.log({
    mensaje: 'Servidor corriendo en puerto 3000'
});
