export interface PersonaInterface {
    id?: string;
    nombre?: string;
    apellido?: string;
    habilitado?: 1 | 0;
}
