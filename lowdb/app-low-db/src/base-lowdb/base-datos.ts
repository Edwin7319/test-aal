const low = require('lowdb');
const fileAsync = require('lowdb/adapters/FileAsync');

let db;

export async function crearConexion() {
    const adapter = new fileAsync('db.json');
    db = await low(adapter);
    db.defaults({
        persona: []
    })
        .write();
}

export const obtenerConexion = () => {
    return db
};

