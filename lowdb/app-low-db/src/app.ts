import * as express from 'express';

const app = express();

app.use(express.json());

app.use(require('./rutas/persona-rutas'));

module .exports = app;
