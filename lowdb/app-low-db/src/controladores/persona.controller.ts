import {obtenerConexion} from '../base-lowdb/base-datos';
import {v4} from 'uuid';
import {PersonaInterface} from "../interface/persona.interface";

export const obetenerPersona = async (req, res) => {
    const arregloPersonas: PersonaInterface[] = await obtenerConexion()
        .get('persona')
        .value();
    res.json(arregloPersonas);
};

export const obtenerPersonaPorId = async (req, res) => {
    const consulta = {
        id: req.params.idPersona
    };
    const persona: PersonaInterface = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .value();

    const existePersona = persona !== undefined;
    if (!existePersona) {
        res.json({
            mensaje: 'No se encontro coincidencia'
        })
    }
    res.json(persona);
};

export const crearPersona = async (req, res) => {
    const persona: PersonaInterface = {
        id: v4(),
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        habilitado: req.body.habilitado
    };

    await obtenerConexion()
        .get('persona')
        .push(persona)
        .write();

    res.send(persona);
};

export const actualizarPersona = async (req, res) => {
    const consulta = {
        id: req.params.idPersona
    };

    const datosActualizados: PersonaInterface = req.body;

    const personaActualizada = await obtenerConexion()
        .get('persona')
        .find(consulta)
        .assign(datosActualizados)
        .write();

    res.json(personaActualizada);
};

export const eliminarPersona = async (req, res) => {
    const consulta = {
        id: req.params.idPersona
    };

    const respuesta = await obtenerConexion()
        .get('persona')
        .remove(consulta)
        .write();

    res.json(respuesta);
};
