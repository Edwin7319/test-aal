import {Router} from 'express'
import {
    obetenerPersona,
    crearPersona,
    obtenerPersonaPorId,
    actualizarPersona,
    eliminarPersona
} from '../controladores/persona.controller';

const router = Router();



router.get('/persona', obetenerPersona);
router.get('/persona/:idPersona', obtenerPersonaPorId);
router.post('/persona', crearPersona);
router.put('/persona/:idPersona', actualizarPersona);
router.delete('/persona/:idPersona', eliminarPersona);

module.exports = router;
